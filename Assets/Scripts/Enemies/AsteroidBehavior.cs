﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the behavior of asteroids in Astro-Shooter
/// </summary>

using UnityEngine;

public class AsteroidBehavior : MonoBehaviour 
{
    // General Asteroid Properties
    public int Health = 0;
    public int Points = 0;
    public int FFPointPenalty = 0;
    public bool IsLootObject = false;

    // Asteroid Speed and Position Properties
    public float LowSpeedRange = 0;
    public float HighSpeedRange = 0;
    public float LowRotationRange = 0;
    public float HighRotationRange = 0;

    // Asteroid Loot Information
    public GameObject LootObject1 = null;
    public GameObject LootObject2 = null;

    // Asteroid Fields
    private float _ySpeed;
    private float _zRotation;
    private bool _lootSpawned;
    private AudioController _gameAudioController;
    private GameControllerBehavior _gameControllerBehavior;
    private Rigidbody2D _asteroidRigidBody;

    /// <summary>
    /// Initializes the asteroid object's fields and starts any appropriate behaviors.
    /// </summary>
    private void Start ()
    {
        // Find the game's AudioController and GameControllerBehavior objects.
        _gameAudioController = GameObject.Find("AudioController").GetComponent<AudioController>();
        _gameControllerBehavior = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();

        // Get the asteroid's rigid body component.
        _asteroidRigidBody = GetComponent<Rigidbody2D>();

        // Roll a dice to see if this asteroid should drop loot.
        DetermineIfLootObject();

        // Set the asteroid's speed from the given range and ensure it is negative (falling).
        _ySpeed = Random.Range(LowSpeedRange, HighSpeedRange);
        _ySpeed = _ySpeed > 0 ? -_ySpeed : _ySpeed;

        // Set the asteroid's rotation.
        _zRotation = Random.Range(LowRotationRange, HighRotationRange);
        transform.Rotate(0f, 0f, _zRotation);

        // Initialize the _lootSpawned variable
        _lootSpawned = false;
	}
	
    /// <summary>
    /// Called once a frame.
    /// Moves the asteroid in accordance to the MoveAsteroid() function.
    /// </summary>
	private void Update ()
    {
        MoveAsteroid();
	}

    /// <summary>
    /// Determines whether or not the asteroid is a loot asteroid with a 1/5 probability that it is.
    /// </summary>
    private void DetermineIfLootObject()
    {
        if (Random.Range(1, 6) == 1)    // 1/5 chance to drop loot
        {
            IsLootObject = true;
        }
        else
        {
            IsLootObject = false;
        }
    }

    /// <summary>
    /// Moves the asteroid down the map.  If the asteroid is past the kill line, then it is destroyed.
    /// </summary>
    private void MoveAsteroid()
    {
        // Check if the game is over.
        if (_gameControllerBehavior.IsGameOver)
        {
            // If it is, destroy the asteroid.
            Destroy(gameObject);
        }
        else if (!_gameControllerBehavior.IsGamePaused)
        {
            // Move the asteroid down by the random speed determined during initialization.
            // The rigid-body is used because asteroids can be rotated and a simple y-axis translation unfortunately doesn't work.
            _asteroidRigidBody.velocity = new Vector2(0f, _ySpeed);

            // Check if the asteroid has past the kill line.  If it is, destroy the asteroid.
            if (transform.position.y < -1.5)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            // Prevent the asteroid from moving.
            _asteroidRigidBody.velocity = new Vector2(0f, 0f);
        }
    }

    /// <summary>
    /// This function checks to see if the asteroid has been destroyed (Health <= 0).
    /// If it has, the function plays the destruction animation, drops a loot item if the asteroid is a loot object, and rewards the player with points.
    /// </summary>
    public void CheckIfDestroyed()
    {
        // Check if the asteroid has been destroyed.
        if (Health < 1)
        {
            // If the asteroid is a loot object, drop a random loot item.
            if (IsLootObject && !_lootSpawned)
            {
                // The below is a switch statement as it makes it easier to add more loot items in the future.
                switch (Random.Range(1, 3))
                {
                    case 1:
                        Instantiate(LootObject1, transform.position, Quaternion.identity);
                        break;
                    case 2:
                        Instantiate(LootObject2, transform.position, Quaternion.identity);
                        break;
                    default:
                        break;
                }

                // Keep track that a kit was already spawned to prevent an item duplication glitch.
                _lootSpawned = true;
            }

            // Play the asteroid explosion animation.
            // The asteroid will be destroyed automatically at the end of the animation.
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            gameObject.GetComponent<Animator>().Play("Asteroid Explosion");

            // Disable the collider so that the player can't kill themselves with an asteroid that should be dead.
            gameObject.GetComponent<CircleCollider2D>().enabled = false;

            // Reward the player the appropriate amount of points.
            GameObject.Find("Player").GetComponent<PlayerBehavior>().Score += Points;
        }
    }

    /// <summary>
    /// Automatically called by the editor.
    /// Runs the appropriate game logic depending on what object collided with the asteroid.
    /// </summary>
    /// <param name="ObjectCollidedWith">The object that collided with this asteroid</param>
    private void OnTriggerEnter2D(Collider2D ObjectCollidedWith)
    {
        switch (ObjectCollidedWith.gameObject.tag)
        {
            case "Asteroid":
                // Play a collision sound effect (only one!).
                _gameAudioController.PlayAudioClip(ClipType.AsteroidExplosion);

                // Penalize the player
                int playerScore = GameObject.Find("Player").GetComponent<PlayerBehavior>().Score;
                playerScore -= FFPointPenalty;
                if (playerScore <= 0)
                {
                    GameObject.Find("Player").GetComponent<PlayerBehavior>().Score = 0;
                }
                else
                {
                    GameObject.Find("Player").GetComponent<PlayerBehavior>().Score = playerScore;
                }

                // Destroy this asteroid and the colliding asteroid
                AsteroidBehavior otherAsteroid = ObjectCollidedWith.GetComponent<AsteroidBehavior>();
                Health = 0;
                otherAsteroid.Health = 0;

                CheckIfDestroyed();
                ObjectCollidedWith.GetComponent<AsteroidBehavior>().CheckIfDestroyed();
                break;

            case "DroidLaser":
                // Play a collision sound effect.
                _gameAudioController.PlayAudioClip(ClipType.AsteroidExplosion);

                // Minus the health of the asteroid by the laser's damage amount
                Health -= ObjectCollidedWith.gameObject.GetComponent<DroidLaserBehavior>().Damage;

                // Play the red flash animation
                gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Destroy the laser
                Destroy(ObjectCollidedWith.gameObject);

                // Check if the asteroid has been destroyed
                CheckIfDestroyed();
                break;

            case "Player":
                // Play a player explosion sound.
                _gameAudioController.PlayAudioClip(ClipType.PlayerDamage);

                // Get a reference to the player's script.
                PlayerBehavior player = GameObject.Find("Player").GetComponent<PlayerBehavior>();

                // Minus 1 life from the player.
                player.GetComponent<PlayerBehavior>().Lives -= 1;

                // Play the player damaged animation
                player.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Destroy the asteroid.
                Destroy(gameObject);
                break;

            case "PlayerLaser":
                // Play a collision sound effect.
                _gameAudioController.PlayAudioClip(ClipType.AsteroidExplosion);

                // Minus the health of the asteroid by the laser's damage amount
                Health -= ObjectCollidedWith.gameObject.GetComponent<PlayerLaserBehavior>().Damage;

                // Play the red flash animation
                gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Destroy the laser
                Destroy(ObjectCollidedWith.gameObject);

                // Check if the asteroid has been destroyed
                CheckIfDestroyed();
                break;

            default:
                break;
        }
    }
}