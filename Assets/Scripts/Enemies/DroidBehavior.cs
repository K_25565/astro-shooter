/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the behaviors of the droid enemy in Astro-Shooter.
/// </summary>

using UnityEngine;

public class DroidBehavior : MonoBehaviour
{
    // Properties
    public int Health = 0;
    public int Points = 0;
    public int FFPointPenalty = 0;
    public float Speed = 0;
    public bool MovingRight = true;
    public GameObject Weapon = null;

    // Fields
    private float _xSpeed;
    private float _ySpeed;
    private float _hoverPoint;
    private int _weaponCoolDownCounter;
    private AudioController _gameAudioController;
    private GameControllerBehavior _gameController;
    private GameObject _player;

    /// <summary>
    /// Initializes the droid enemy.
    /// </summary>
    private void Start()
    {
        // Initialize the speed fields of the droid
        _xSpeed = Speed;
        _ySpeed = Speed < 0 ? Speed : -Speed;

        // Calculate the droid's random "hover-point"
        _hoverPoint = Random.Range(-0.7f, 0.7f);

        // Initialize the droid's weapon cool-down counter
        _weaponCoolDownCounter = int.MaxValue;

        // Get a reference to the game audio controller, game controller, and the player.
        _gameAudioController = GameObject.Find("AudioController").GetComponent<AudioController>();
        _gameController = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();
        _player = GameObject.Find("Player");
    }

    /// <summary>
    /// Called automatically by the editor once per frame.
    /// Moves the droid and tells the droid when to fire its weapon as appropriate.
    /// </summary>
    private void Update()
    {
        MoveDroid();
        FireWeapon();
    }

    /// <summary>
    /// Moves the droid down until it reaches its hover point.
    /// Then, the droid is moved right until a border is hit.  When a border is hit, the droid moves left until another border is hit, where it goes back to moving right.
    /// </summary>
    private void MoveDroid()
    {
        // Check if the game is over.
        if (_gameController.IsGameOver)
        {
            // Destroy the droid as the game is over.
            Destroy(gameObject);
        }
        else if (!_gameController.IsGamePaused)
        {
            // Check if the droid is at its hover point.
            if (transform.position.y >= _hoverPoint)
            {
                // Move down until the hover point is reached.
                transform.Translate(0f, _ySpeed * Time.deltaTime, 0f);
            }
            else
            {
                // Once the hover point is met, go right until a border is hit, then left until a border is hit.
                if (MovingRight)
                {
                    transform.Translate(_xSpeed * Time.deltaTime, 0f, 0f);
                }
                else
                {
                    transform.Translate(-_xSpeed * Time.deltaTime, 0f, 0f);
                }
            }
        }
    }

    /// <summary>
    /// Allows the droid to shoot a laser at the player depending on two main conditions: randomly or if the player is nearby.
    /// Uses a cool down counter so that the droid doesn't spam lasers when the player stays close to it.
    /// </summary>
    private void FireWeapon()
    {
        // Ensure the game isn't over
        if (!_gameController.IsGameOver && !_gameController.IsGamePaused)
        {
            // Check if the droid should randomly shoot 1/1000 chance
            if (Random.Range(1, 1001) == 12)
            {
                // Shoot!
                SpawnDroidLaser();
            }
            // Check if the player is near and the droid isn't in its cool down phase
            else if (Mathf.Abs(_player.transform.position.x - transform.position.x) < 0.1 && _weaponCoolDownCounter == 0)
            {
                // Reset the weapon cool-down
                _weaponCoolDownCounter = int.MaxValue;

                // Shoot!
                SpawnDroidLaser();
            }
            else
            {
                // Decrement the cool down counter by 1.
                _weaponCoolDownCounter--;
            }
        }
    }

    /// <summary>
    /// Spawn's one of the droid's laser 0.125 spaces below the droid.
    /// </summary>
    private void SpawnDroidLaser()
    {
        // Set the location to spawn the laser at 0.125 lower than the droid.
        Vector2 weaponLocation = new Vector2(transform.position.x, transform.position.y - 0.125f);

        // Play a random laser sound effect
        _gameAudioController.PlayAudioClip(ClipType.DroidLaser);

        // Spawn a new laser object
        Instantiate(Weapon, weaponLocation, Quaternion.identity);
    }

    /// <summary>
    /// Checks to see if this droid has been destroyed.
    /// </summary>
    private bool CheckIfDestroyed()
    {
        if (Health < 1)
        {
            // Let the game controller know a droid was destroyed so it can spawn a replacement droid.
            _gameController.DecrementDroidCount();

            // Play the droid explosion animation.
            // The droid will be destroyed automatically at the end of the animation.
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            gameObject.GetComponent<Animator>().Play("Droid Explosion");

            // Disable the collider so that the player can't kill themselves with an droid that should be dead.
            gameObject.GetComponent<BoxCollider2D>().enabled = false;

            // Reward the player the appropriate amount of points.
            GameObject.Find("Player").GetComponent<PlayerBehavior>().Score += Points;

            // Return true as the droid has been destroyed
            return true;
        }
        else
        {
            // Return true as the droid hasn't been destroyed
            return false;
        }
    }

    /// <summary>
    /// Automatically called by the editor.
    /// Runs the appropriate game logic depending on what object collided with the droid.
    /// </summary>
    /// <param name="objectCollidedWith">The object that collided with this droid</param>
    private void OnTriggerEnter2D(Collider2D objectCollidedWith)
    {
        switch (objectCollidedWith.gameObject.tag)
        {
            case "Asteroid":
                // Play a droid damaged sound effect
                _gameAudioController.PlayAudioClip(ClipType.DroidDamage);

                // Decrement the droid's health by 10.
                Health -= 10;

                // Play the red flash animation
                gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Destroy the colliding object
                AsteroidBehavior asteroid = objectCollidedWith.GetComponent<AsteroidBehavior>();
                asteroid.Health = 0;
                asteroid.CheckIfDestroyed();

                // Check if the droid has been destroyed.
                CheckIfDestroyed();
                break;

            case "Border":
                // Get the ID of the border collided with.
                string borderDirection = objectCollidedWith.GetComponent<BorderBehavior>().BorderDirection;

                // Based off of the border's direction ID, change movement direction as appropriate.
                if (borderDirection == "East")
                {
                    MovingRight = false;
                }
                else if (borderDirection == "West")
                {
                    MovingRight = true;
                }
                break;

            case "Droid":
                // Play a droid damaged sound effect
                _gameAudioController.PlayAudioClip(ClipType.DroidDamage);

                // Get an easy reference to the other droid.
                DroidBehavior otherDroid = objectCollidedWith.GetComponent<DroidBehavior>();

                // Decrement the both of the droids' health by 10.
                Health -= 10;
                otherDroid.Health -= 10;

                // Change the movement directions of both droids
                MovingRight = !MovingRight;
                otherDroid.MovingRight = !otherDroid.MovingRight;

                // Play the red flash animation
                gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Check if either droid has been destroyed.  If one has, then penalize the player.
                if (CheckIfDestroyed() || otherDroid.CheckIfDestroyed())
                {
                    // Penalize the player.
                    int playerScore = _player.GetComponent<PlayerBehavior>().Score;
                    playerScore -= FFPointPenalty;
                    if (playerScore <= 0)
                    {
                        _player.GetComponent<PlayerBehavior>().Score = 0;
                    }
                    else
                    {
                        _player.GetComponent<PlayerBehavior>().Score = playerScore;
                    }
                }
                break;

            case "DroidLaser":
                // Play a droid damaged sound effect
                _gameAudioController.PlayAudioClip(ClipType.DroidDamage);

                // Decrement the droid's health by 10.
                Health -= 10;

                // Play the red flash animation
                gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Destroy the colliding object
                Destroy(objectCollidedWith.gameObject);

                // Check if the droid has been destroyed.  If it has, then penalize the player.
                if (CheckIfDestroyed())
                {
                    // Penalize the player.
                    int playerScore = _player.GetComponent<PlayerBehavior>().Score;
                    playerScore -= FFPointPenalty;
                    if (playerScore <= 0)
                    {
                        _player.GetComponent<PlayerBehavior>().Score = 0;
                    }
                    else
                    {
                        _player.GetComponent<PlayerBehavior>().Score = playerScore;
                    }
                }
                break;

            case "Player":
                // Play a random player damage sound effect
                _gameAudioController.PlayAudioClip(ClipType.PlayerDamage);

                // Minus 1 life from the player
                _player.GetComponent<PlayerBehavior>().Lives -= 1;

                // Let the game controller know a droid was destroyed so it can spawn a replacement droid.
                _gameController.DecrementDroidCount();

                // Play the red flash animation
                gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Destroy the droid.
                Health = 0;
                CheckIfDestroyed();
                break;

            case "PlayerLaser":
                // Play a droid damaged sound effect
                _gameAudioController.PlayAudioClip(ClipType.DroidDamage);

                // Decrement the droid's health by 10.
                Health -= 10;

                // Play the red flash animation
                gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

                // Destroy the colliding object
                Destroy(objectCollidedWith.gameObject);

                // Check if the droid has been destroyed.
                CheckIfDestroyed();
                break;

            default:
                break;
        }
    }
}