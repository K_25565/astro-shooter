﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/21/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the behavior of asteroids in Astro-Shooter
/// </summary>

using UnityEngine;

public class SkyboxBehavior : MonoBehaviour 
{
    // Properties
    public float Speed = 0;
    public float TeleportYCoordinate = 0;
    public float TriggerYCoordinate = 0;

    // Fields
    private float _ySpeed;
    private GameControllerBehavior _gameController;
    
    /// <summary>
    /// Initializes the sky-box
    /// </summary>
	private void Start()
    {
        // Set the sky-box's speed and ensure it is negative (falling).
        _ySpeed = Speed > 0 ? -Speed : Speed;

        // Set a reference to the game controller
        _gameController = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();
    }
	
	/// <summary>
    /// Called once per frame.
    /// Moves the sky-box as appropriate.
    /// </summary>
	private void Update()
    {
        MoveSkybox();
	}

    /// <summary>
    /// Function that moves the sky-box to give the illusion of movement within the game.
    /// </summary>
    private void MoveSkybox()
    {
        // Make sure the game isn't over.
        if (!_gameController.IsGameOver && !_gameController.IsGamePaused)
        {
            // Check if the sky-box is below the TriggerYCoordinate.
            if (gameObject.transform.position.y < TriggerYCoordinate)
            {
                // Move the sky-box to the TeleportYCoordinate.
                gameObject.transform.position = new Vector2(0f, TeleportYCoordinate);
            }
            else
            {
                // Move the sky-box down by the user defined speed.
                transform.Translate(0f, _ySpeed * Time.deltaTime, 0f);
            }
        }
    }
}