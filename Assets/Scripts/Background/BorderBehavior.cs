﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/21/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the behavior of asteroids in Astro-Shooter
/// </summary>

using UnityEngine;

public class BorderBehavior : MonoBehaviour 
{
    /// <summary>
    /// Property that contains the direction of the border this script is associated with.
    /// </summary>
    public string BorderDirection = string.Empty;

    /// <summary>
    /// Automatically called by the editor.
    /// Disable's the player's ability to go any further in this border's direction when the player collides with the border.
    /// </summary>
    /// <param name="ObjectCollidedWith">The object that collided into the border</param>
    private void OnTriggerEnter2D(Collider2D ObjectCollidedWith)
    {
        // Check if the player ran into the collider
        if (ObjectCollidedWith.gameObject.name == "Player")
        {
            // Disable the player's ability to go further
            TogglePlayerControl(BorderDirection, false);
        }
    }

    /// <summary>
    /// Automatically called by the editor.
    /// Enable's the player's ability to go further in this border's direction when the player stops colliding with the border.
    /// </summary>
    /// <param name="ObjectLeft">The object that stopped colliding with the border</param>
    private void OnTriggerExit2D(Collider2D ObjectLeft)
    {
        // Check if the player left the collider
        if (ObjectLeft.gameObject.name == "Player")
        {
            // Re-enable the player's ability to go further
            TogglePlayerControl(BorderDirection, true);
        }
    }

    /// <summary>
    /// Modifies the player's control given a direction and a new state that the player's control should be.
    /// </summary>
    /// <param name="direction">The direction of player control that is being modified</param>
    /// <param name="controlState">The new state of the player's directional control</param>
    private void TogglePlayerControl(string direction, bool newControlState)
    {
        // Create a reference to the player script
        PlayerBehavior player = GameObject.Find("Player").GetComponent<PlayerBehavior>();

        switch(direction)
        {
            case "North":
                player.CanMoveForward = newControlState;
                break;
            case "East":
                player.CanMoveRight = newControlState;
                break;
            case "South":
                player.CanMoveBack = newControlState;
                break;
            case "West":
                player.CanMoveLeft = newControlState;
                break;
            default:
                print("An error has occurred in BorderBehavior.cs");
                break;
        }
    }
}