﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/21/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the behavior of the player's laser in Astro-Shooter
/// </summary>

using UnityEngine;

public class PlayerLaserBehavior : LaserBehavior
{
    /// <summary>
    /// Initializes the laser's fields and starts any appropriate behaviors.
    /// </summary>
    protected override void Start()
    {
        // Setup the game controller reference
        _gameController = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();

        // Set the laser's speed from the given range and ensure it is positive (rising).
        _ySpeed = Random.Range(LowerSpeedRange, UpperSpeedRange);
        _ySpeed = _ySpeed > 0 ? _ySpeed : -_ySpeed;
    }
}