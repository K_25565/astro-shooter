/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/21/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the common behavior of all lasers in Astro-Shooter
/// </summary>

using UnityEngine;

public abstract class LaserBehavior : MonoBehaviour
{
    // Properties
    public int Damage = 0;
    public float LowerSpeedRange = 0;
    public float UpperSpeedRange = 0;

    // Fields
    protected float _ySpeed;
    protected GameControllerBehavior _gameController;

    /// <summary>
    /// Initializes the laser's fields and starts any appropriate behaviors.
    /// </summary>
    protected abstract void Start();

    /// <summary>
    /// Called once a frame.
    /// Moves the laser in accordance to the MoveLaser() function.
    /// </summary>
    protected void Update()
    {
        MoveLaser();
    }

    /// <summary>
    /// Moves the laser up.  If the laser is above the kill line, then it is destroyed.
    /// </summary>
    protected void MoveLaser()
    {
        // Check if game over mode is enabled
        if (_gameController.IsGameOver)
        {
            // Destroy the laser
            Destroy(gameObject);
        }
        else if (!_gameController.IsGamePaused)
        {
            // Move the laser forward by the generated random speed
            transform.Translate(0f, _ySpeed * Time.deltaTime, 0f);

            // Check if the laser is past the kill line (upper or lower)
            if (transform.position.y > 1.5 || transform.position.y < -1.5)
            {
                // If it is, destroy it
                Destroy(gameObject);
            }
        }
    }
}