﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/20/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that controls the behavior of the score UI element of Astro-Shooter
/// </summary>

using UnityEngine;
using UnityEngine.UI;

public class ScoreTextBehavior : MonoBehaviour 
{
    // Fields
    private PlayerBehavior _player;
    private Text _scoreText;

    /// <summary>
    /// Initializes a reference to the player object and text component of this UI element so that they aren't constantly being searched for.
    /// </summary>
    private void Start()
    {
        _player = GameObject.Find("Player").GetComponent<PlayerBehavior>();
        _scoreText = GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update ()
    {
        // Set the ScoreText to Score:  concatenated with the player's score
        _scoreText.text = "Score:  " + _player.Score.ToString();
	}
}