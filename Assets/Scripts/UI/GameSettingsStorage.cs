﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/22/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that can store data across Unity scenes.
/// </summary>

public static class GameSettingsStorage
{
    // Default Constants
    public const int   DEFAULT_LIVES      = 3;
    public const int   DEFAULT_SCORE      = 0;
    public const int   DEFAULT_AMMO       = 30;
    public const float DEFAULT_SPAWN_RATE = 1f;

    // Properties
    public static bool  IsCustomGame { get; set; }
    public static int   Lives        { get; set; }
    public static int   Score        { get; set; }
    public static int   Ammo         { get; set; }
    public static float SpawnRate    { get; set; }

    /// <summary>
    /// Resets the GameSettingsStorage to the default values.
    /// </summary>
    public static void ResetSettings()
    {
        IsCustomGame = false;
        Lives = DEFAULT_LIVES;
        Score = DEFAULT_SCORE;
        Ammo = DEFAULT_AMMO;
        SpawnRate = DEFAULT_SPAWN_RATE;
    }
}