﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/22/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that contains functions for UI buttons to run in Astro-Shooter
/// </summary>

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonFunctions : MonoBehaviour 
{
    /// <summary>
    /// Reloads the main game scene (restarts the game).
    /// </summary>
	public void Restart()
    {
        // Reload the current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Resets all of the input fields in the Game Setup to their default values.
    /// </summary>
    public void ResetGameSetup()
    {
        // Set all of the input fields to their default amounts
        GameObject.Find("Starting Ammo Input").GetComponent<InputField>().text = "30";
        GameObject.Find("Max Lives Input").GetComponent<InputField>().text = "3";
        GameObject.Find("Spawn Frequency Input").GetComponent<InputField>().text = "1";
        GameObject.Find("Starting Score Input").GetComponent<InputField>().text = "0";
    }

    /// <summary>
    /// Reloads the leaderboard with a new, blank leaderboard.
    /// </summary>
    public void ResetLeaderboard()
    {
        GameObject.Find("UI").GetComponent<LeaderboardBehavior>().ResetLeaderboard();
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void Quit()
    {
        // Tell the game to quit
        Application.Quit();
    }

    /// <summary>
    /// Loads the Main Menu scene.
    /// </summary>
    public void GoToMainMenu()
    {
        // Load the main menu
        SceneManager.LoadScene("Main Menu");
    }

    /// <summary>
    /// Loads the Game Setup scene.
    /// </summary>
    public void GoToGameSetup()
    {
        // Load the custom game setup
        SceneManager.LoadScene("Game Setup");
    }

    /// <summary>
    /// Loads the Leaderboard scene.
    /// </summary>
    public void GoToLeaderboard()
    {
        // Load the leaderboard
        SceneManager.LoadScene("Leaderboard");
    }

    /// <summary>
    /// Loads the main game scene.
    /// </summary>
    public void GoToMain()
    {
        try
        {
            // Try to convert the inputs to the right types
            GameSettingsStorage.Lives     = int.Parse(GameObject.Find("Max Lives Input").GetComponent<InputField>().text);
            GameSettingsStorage.Score     = int.Parse(GameObject.Find("Starting Score Input").GetComponent<InputField>().text);
            GameSettingsStorage.Ammo      = int.Parse(GameObject.Find("Starting Ammo Input").GetComponent<InputField>().text);
            GameSettingsStorage.SpawnRate = float.Parse(GameObject.Find("Spawn Frequency Input").GetComponent<InputField>().text);
        }
        catch
        {
            // If there is an error, print an error and reset to defaults.
            Debug.LogError("An error has occurred when attempting to convert custom input to the correct data types!  Resetting to defaults values.");
            GameSettingsStorage.ResetSettings();
        }

        // Check to see if any of the default values have changed
        if (GameSettingsStorage.Lives != GameSettingsStorage.DEFAULT_LIVES || GameSettingsStorage.Score != GameSettingsStorage.DEFAULT_SCORE || GameSettingsStorage.Ammo != GameSettingsStorage.DEFAULT_AMMO || GameSettingsStorage.SpawnRate != GameSettingsStorage.DEFAULT_SPAWN_RATE)
        {
            GameSettingsStorage.IsCustomGame = true;
        }
        else
        {
            GameSettingsStorage.IsCustomGame = false;
        }
        
        // Load the main game scene
        SceneManager.LoadScene("Main");
    }
}