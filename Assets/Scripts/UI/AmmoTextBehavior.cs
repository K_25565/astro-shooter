﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/20/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that updates the ammo text in the UI of Astro-Shooter
/// </summary>

using UnityEngine;
using UnityEngine.UI;

public class AmmoTextBehavior : MonoBehaviour 
{
    // Fields
    private PlayerBehavior _player;
    private Text _ammoText;

    /// <summary>
    /// Initializes a reference to the player object and text component of this UI element so that they aren't constantly being searched for.
    /// </summary>
    private void Start()
    {
        _player = GameObject.Find("Player").GetComponent<PlayerBehavior>();
        _ammoText = GetComponent<Text>();
    }

    /// <summary>
    /// Called once per frame.
    /// Updates the Ammo UI text with the actual value of the player's ammo.
    /// </summary>
    private void Update()
    {
        // Set the Ammo Text to Ammo:  concatenated with the ammo left
        _ammoText.text = "Ammo:  " + _player.Ammo.ToString();
	}
}