/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/22/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that controls the behavior of the pause menu of Astro-Shooter
/// </summary>

using UnityEngine;

public class PauseMenuBehavior : MonoBehaviour
{
    // Fields
    private GameControllerBehavior _gameController;

    /// <summary>
    /// Initializes a reference to the game controller so that it isn't being searched for each frame.
    /// </summary>
    private void Start()
    {
        _gameController = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();
    }

    /// <summary>
    /// Called once per frame
    /// Show the Pause menu if the game is paused.  Otherwise hide it.
    /// (This is an unoptimized way to do this as each frame the menu is either hidden or shown.)
    /// </summary>
    private void Update()
    {
        // Check if the game is over
        if (_gameController.IsGamePaused)
        {
            // Enable all of these UI Elements
            UIUtilities.SetUIElementState("Pause Menu", "Image", true);
            UIUtilities.SetUIElementState("Paused Text", "Text", true);
            UIUtilities.SetUIElementState("Paused Restart Button", "Button", true);
            UIUtilities.SetUIElementState("Paused Restart Button", "Image", true);
            UIUtilities.SetUIElementState("Paused Restart Button Text", "Text", true);
            UIUtilities.SetUIElementState("Paused Leaderboard Button", "Button", true);
            UIUtilities.SetUIElementState("Paused Leaderboard Button", "Image", true);
            UIUtilities.SetUIElementState("Paused Leaderboard Button Text", "Text", true);
            UIUtilities.SetUIElementState("Paused Main Menu Button", "Button", true);
            UIUtilities.SetUIElementState("Paused Main Menu Button", "Image", true);
            UIUtilities.SetUIElementState("Paused Main Menu Button Text", "Text", true);
            UIUtilities.SetUIElementState("Paused Quit Button", "Button", true);
            UIUtilities.SetUIElementState("Paused Quit Button", "Image", true);
            UIUtilities.SetUIElementState("Paused Quit Button Text", "Text", true);
        }
        else
        {
            // Disable all of these UI Elements
            UIUtilities.SetUIElementState("Pause Menu", "Image", false);
            UIUtilities.SetUIElementState("Paused Text", "Text", false);
            UIUtilities.SetUIElementState("Paused Restart Button", "Button", false);
            UIUtilities.SetUIElementState("Paused Restart Button", "Image", false);
            UIUtilities.SetUIElementState("Paused Restart Button Text", "Text", false);
            UIUtilities.SetUIElementState("Paused Leaderboard Button", "Button", false);
            UIUtilities.SetUIElementState("Paused Leaderboard Button", "Image", false);
            UIUtilities.SetUIElementState("Paused Leaderboard Button Text", "Text", false);
            UIUtilities.SetUIElementState("Paused Main Menu Button", "Button", false);
            UIUtilities.SetUIElementState("Paused Main Menu Button", "Image", false);
            UIUtilities.SetUIElementState("Paused Main Menu Button Text", "Text", false);
            UIUtilities.SetUIElementState("Paused Quit Button", "Button", false);
            UIUtilities.SetUIElementState("Paused Quit Button", "Image", false);
            UIUtilities.SetUIElementState("Paused Quit Button Text", "Text", false);
        }
    }
}