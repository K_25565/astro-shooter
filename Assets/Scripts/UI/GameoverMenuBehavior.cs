/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that controls the behavior of the game over menu of Astro-Shooter
/// </summary>

using UnityEngine;

public class GameOverMenuBehavior : MonoBehaviour
{
    // Fields
    private GameControllerBehavior _gameController;

    /// <summary>
    /// Initializes a reference to the game controller so that it isn't being searched for each frame.
    /// </summary>
    private void Start()
    {
        _gameController = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();
    }

    /// <summary>
    /// Called once per frame
    /// Show the GameOver menu if the game is over.  Otherwise hide it.
    /// (This is an unoptimized way to do this as each frame the menu is either hidden or shown.)
    /// </summary>
    private void Update()
    {
        // Check if the game is over
        if (_gameController.IsGameOver)
        {
            // Enable all of these UI Elements
            UIUtilities.SetUIElementState("Game Over Message Panel", "Image", true);
            UIUtilities.SetUIElementState("Game Over Text", "Text", true);
            UIUtilities.SetUIElementState("Restart Button", "Button", true);
            UIUtilities.SetUIElementState("Restart Button", "Image", true);
            UIUtilities.SetUIElementState("Restart Button Text", "Text", true);
            UIUtilities.SetUIElementState("Leaderboard Button", "Button", true);
            UIUtilities.SetUIElementState("Leaderboard Button", "Image", true);
            UIUtilities.SetUIElementState("Leaderboard Button Text", "Text", true);
            UIUtilities.SetUIElementState("Main Menu Button", "Button", true);
            UIUtilities.SetUIElementState("Main Menu Button", "Image", true);
            UIUtilities.SetUIElementState("Main Menu Button Text", "Text", true);
            UIUtilities.SetUIElementState("Quit Button", "Button", true);
            UIUtilities.SetUIElementState("Quit Button", "Image", true);
            UIUtilities.SetUIElementState("Quit Button Text", "Text", true);
        }
        else
        {
            // Disable all of these UI Elements
            UIUtilities.SetUIElementState("Game Over Message Panel", "Image", false);
            UIUtilities.SetUIElementState("Game Over Text", "Text", false);
            UIUtilities.SetUIElementState("Restart Button", "Button", false);
            UIUtilities.SetUIElementState("Restart Button", "Image", false);
            UIUtilities.SetUIElementState("Restart Button Text", "Text", false);
            UIUtilities.SetUIElementState("Leaderboard Button", "Button", false);
            UIUtilities.SetUIElementState("Leaderboard Button", "Image", false);
            UIUtilities.SetUIElementState("Leaderboard Button Text", "Text", false);
            UIUtilities.SetUIElementState("Main Menu Button", "Button", false);
            UIUtilities.SetUIElementState("Main Menu Button", "Image", false);
            UIUtilities.SetUIElementState("Main Menu Button Text", "Text", false);
            UIUtilities.SetUIElementState("Quit Button", "Button", false);
            UIUtilities.SetUIElementState("Quit Button", "Image", false);
            UIUtilities.SetUIElementState("Quit Button Text", "Text", false);
        }
    }
}