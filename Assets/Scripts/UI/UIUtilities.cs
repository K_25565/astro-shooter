/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/21/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that can store data across Unity scenes.
/// </summary>

using UnityEngine;
using UnityEngine.UI;

public static class UIUtilities
{
    /// <summary>
    /// Sets the state of the given UI element.
    /// </summary>
    /// <param name="gameObjectToToggle">The UI element to set the state of.</param>
    /// <param name="uiElementType">The type of the UI element to toggle represented as a string.</param>
    /// <param name="state">The new state of the UI element.</param>
    public static void SetUIElementState(string gameObjectToToggle, string uiElementType, bool state)
    {
        switch (uiElementType)
        {
            case "Image":
                GameObject.Find(gameObjectToToggle).GetComponent<Image>().enabled = state;
                break;

            case "Text":
                GameObject.Find(gameObjectToToggle).GetComponent<Text>().enabled = state;
                break;

            case "Button":
                GameObject.Find(gameObjectToToggle).GetComponent<Button>().enabled = state;
                break;
        }
    }
}