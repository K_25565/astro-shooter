﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/20/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that controls the behavior of the lives UI element of Astro-Shooter
/// </summary>

using UnityEngine;
using UnityEngine.UI;

public class LifeTextBehavior : MonoBehaviour 
{
    // Properties
    public int LifeID = 0;

    // Fields
    private PlayerBehavior _player;
    private Image _image;

    /// <summary>
    /// Initializes a reference to the player object and image component of this UI element so that they aren't constantly being searched for.
    /// </summary>
    private void Start()
    {
        _player = GameObject.Find("Player").GetComponent<PlayerBehavior>();
        _image = GetComponent<Image>();
    }

    /// <summary>
    /// Runs every frame automatically.
    /// Updates the UI to show the appropriate amount of lives the player has.
    /// </summary>
    private void Update()
    {
        // Check if the player's life amount is lower than that of this id's
        if (_player.Lives < LifeID)
        {
            // Hide the life image
            _image.enabled = false;
        }
        else
        {
            // Show the life image
            _image.enabled = true;
        }
    }
}