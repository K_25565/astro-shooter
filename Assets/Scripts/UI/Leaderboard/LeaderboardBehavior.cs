/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/22/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that controls the behavior of the leaderboard in Astro-Shooter.
/// </summary>

using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderboardBehavior : MonoBehaviour
{
    // Properties
    public GameObject LeaderboardEntry = null;

    // Fields
    [SerializeField]
    private Transform _scrollViewContent;
    private List<GameObject> _leaderboardEntries;

    // Start is called before the first frame update
    private void Start()
    {
        // Ensure that the location of the leaderboard file is properly set.
        LeaderboardManager.FilePath = $"{Application.persistentDataPath}/";
        LeaderboardManager.FileName = "leaderboard.astro";

        // Initialize the list that keeps track of the leaderboard game objects.
        _leaderboardEntries = new List<GameObject>();

        // Load the leaderboard.
        LoadLeaderboard();
    }

    /// <summary>
    /// Removes every item from the leaderboard.
    /// </summary>
    private void RemoveAllItemsFromLeaderboard()
    {
        // Delete each LeaderboardEntry object in the game.
        foreach(GameObject leaderboardEntry in _leaderboardEntries)
        {
            Destroy(leaderboardEntry.gameObject);
        }
    }

    /// <summary>
    /// Reads the leaderboard file and populates the leaderboard with the contents of the file.
    /// </summary>
    private void LoadLeaderboard()
    {
        int leaderboardIndex = 1;
        foreach (LeaderboardEntry entry in LeaderboardManager.ReadFromLeaderboard())
        {
            // Create a leaderboardEntry object and add it to the list of leaderboardEntry objects
            GameObject leaderboardEntry = Instantiate(LeaderboardEntry, _scrollViewContent);
            _leaderboardEntries.Add(leaderboardEntry);

            // Format the leaderboardEntry object's text.
            leaderboardEntry.GetComponent<TMP_Text>().text = $"{leaderboardIndex} - {entry.Date.ToString("MM / dd / yyyy")} at {entry.Date.ToString("h:mm tt")} - {entry.Score}";
            
            // Increment the leaderboard index by 1.
            leaderboardIndex++;
        }
    }

    /// <summary>
    /// Clears the leaderboard display and regenerates the leaderboard file.
    /// </summary>
    public void ResetLeaderboard()
    {
        RemoveAllItemsFromLeaderboard();
        LeaderboardManager.GenerateNewLeaderboard();
    }
}