/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/22/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that manages reads and writes to the leaderboard file.
/// </summary>

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

public static class LeaderboardManager
{
    // Fields
    private static string _filePath;
    private static string _fileName;
    private static bool _filePathSet = false;
    private static bool _fileNameSet = false;

    /// <summary>
    /// Property that stores the leaderboard file's path.
    /// If this property isn't set before attempting a get, then an IOException will be thrown.
    /// </summary>
    /// <exception cref="IOException">Throws if the property isn't set before getting this property's value.</exception>
    public static string FilePath 
    { 
        get
        {
            if (_filePathSet)
            {
                return _filePath;
            }    
            else
            {
                throw new IOException("The file path to save the leaderboard to has not been set!");
            }
        }
        set
        {
            _filePath = value;
            _filePathSet = true;
        }
    }

    /// <summary>
    /// Property that stores the leaderboard file's name.
    /// If this property isn't set before attempting a get, then an IOException will be thrown.
    /// </summary>
    /// <exception cref="IOException">Throws if the property isn't set before getting this property's value.</exception>
    public static string FileName 
    { 
        get
        {
            if (_fileNameSet)
            {
                return _fileName;
            }
            else
            {
                throw new IOException("The file name of the leaderboard file has not been set!");
            }
        }
        set
        {
            _fileName = value;
            _fileNameSet = true;
        }
    }

    /// <summary>
    /// Creates a new leaderboard file.
    /// If the leaderboard's FilePath or FileName haven't been set, then an IOException will be thrown.
    /// If the LeaderboardManager can somehow attempt to read a file that doesn't exist, then a FileNotFoundException is thrown.
    /// </summary>
    /// <param name="score"></param>
    /// <exception cref="IOException">Throws if either the FilePath or FileName properties haven't been set.</exception>
    public static void GenerateNewLeaderboard()
    {
        // Create the save path.
        // If either the FilePath or FileName haven't been set, this will throw an error.
        string savePath = FilePath + FileName;

        // Create an empty leaderboard file.
        FileStream file = File.OpenWrite(savePath);
        BinaryFormatter dataFormatter = new BinaryFormatter();
        dataFormatter.Serialize(file, new List<LeaderboardEntry>());
        file.Close();
    }

    /// <summary>
    /// Saves the given score to the leaderboard file.
    /// If the leaderboard's FilePath or FileName haven't been set, then an IOException will be thrown.
    /// If the LeaderboardManager can somehow attempt to read a file that doesn't exist, then a FileNotFoundException is thrown.
    /// </summary>
    /// <param name="score"></param>
    /// <exception cref="IOException">Throws if either the FilePath or FileName properties haven't been set.</exception>
    /// <exception cref="FileNotFoundException">Throws if the LeaderboardManager attempts to read a file that doesn't exist.</exception>
    public static void WriteToLeaderboard(int score)
    {
        // Create the save path.
        // If either the FilePath or FileName haven't been set, this will throw an error.
        string savePath = FilePath + FileName;

        // Check if the leaderboard file already exists.  If it does, read it and add the new entry.  Otherwise, just add the new entry.
        List<LeaderboardEntry> unsortedEntries = File.Exists(savePath) ? ReadFromLeaderboard() : new List<LeaderboardEntry>();
        unsortedEntries.Add(new LeaderboardEntry(DateTime.Now, score));

        // Sort the leaderboard list by the player's score (so that we don't have to worry about sorting later.
        List<LeaderboardEntry> sortedEntries = unsortedEntries.OrderByDescending(e => e.Score).ToList();

        // Save the sorted leaderboard data to the savePath
        FileStream file = File.OpenWrite(savePath);
        BinaryFormatter dataFormatter = new BinaryFormatter();
        dataFormatter.Serialize(file, sortedEntries);
        file.Close();
    }

    /// <summary>
    /// Reads and returns the leaderboard file as a list.
    /// If the leaderboard's FilePath or FileName haven't been set, then an IOException will be thrown.
    /// If the LeaderboardManager attempts to read a file that doesn't exist, then a FileNotFoundException is thrown.
    /// </summary>
    /// <returns>The contents of the leaderboard file as a list</returns>
    /// <exception cref="FileNotFoundException"></exception>
    public static List<LeaderboardEntry> ReadFromLeaderboard()
    {
        // Create the save path.
        // If either the FilePath or FileName haven't been set, this will throw an error.
        string savePath = FilePath + FileName;

        // If a leaderboard file doesn't exist, generate a new one.
        if (!File.Exists(savePath))
        {
            //Debug.Log($"The leaderboard file {savePath} was not found.  Generating new leaderboard file!");
            GenerateNewLeaderboard();
        }

        // Attempt to read the leaderboard file.
        FileStream file = File.Exists(savePath) ? File.OpenRead(savePath) : throw new FileNotFoundException("The requested file was not found.", savePath);
        BinaryFormatter dataFormatter = new BinaryFormatter();
        List<LeaderboardEntry> entries = (List<LeaderboardEntry>) dataFormatter.Deserialize(file);
        file.Close();

        // Return the contents of the leaderboard file to the caller.
        return entries;
    }
}