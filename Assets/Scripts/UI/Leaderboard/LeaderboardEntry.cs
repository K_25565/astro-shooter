/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/22/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents a single entry in the leaderboard.
/// </summary>

using System;

[Serializable]
public class LeaderboardEntry
{
    // Properties
    public DateTime Date { get; set; }
    public int Score { get; set; }

    /// <summary>
    /// Constructor for LeaderboardEntry objects
    /// </summary>
    /// <param name="entryDate">The date of the leaderboard entry.</param>
    /// <param name="score">The score of the leaderboard entry.</param>
    public LeaderboardEntry(DateTime entryDate, int score)
    {
        Date = entryDate;
        Score = score;
    }
}