﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/20/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that automatically destroys objects when they are done animating.
/// </summary>

using UnityEngine;

public class AnimationAutoDestroy : StateMachineBehaviour
{
    /// <summary>
    /// Automatically destroys the attached object at the end of the animation
    /// </summary>
    /// <param name="animator">The object being animated</param>
    /// <param name="stateInfo">Information about the animation's state</param>
    /// <param name="layerIndex">The animation layer index</param>
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        Destroy(animator.gameObject, stateInfo.length);
    }
}