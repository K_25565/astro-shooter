/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the behavior for the red flash animation in Astro-Shooter.
/// </summary>

using System.Collections;
using UnityEngine;

public class RedFlashAnimationBehavior : MonoBehaviour
{
    /// <summary>
    /// Changes the color of the object from white to read to white for 0.2 seconds.
    /// </summary>
    public void PlayAnimation()
    {
        StartCoroutine(FlashDroidColor());
    }

    /// <summary>
    /// Changes the color of the object from white to red to white.
    /// </summary>
    /// <returns>Uses Unity's WaitForSeconds. Nothing is actually returned.</returns>
    private IEnumerator FlashDroidColor()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
        yield return new WaitForSecondsRealtime(0.20f);
        gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
    }
}