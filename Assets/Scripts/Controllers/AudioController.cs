﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the behavior of the AudioController in Astro-Shooter along with an enumerable definition of audio clip types.
/// </summary>

using System.Collections.Generic;
using UnityEngine;

public enum ClipType : ushort
{
    AsteroidExplosion,
    DroidDamage,
    DroidLaser,
    KitSelection,
    PlayerDamage,
    PlayerLaser
}

public class AudioController : MonoBehaviour 
{
    // Properties
    public AudioClip AsteroidExplosion1 = null;
    public AudioClip AsteroidExplosion2 = null;
    public AudioClip AsteroidExplosion3 = null;
    public AudioClip AsteroidExplosion4 = null;
    public AudioClip AsteroidExplosion5 = null;
    public AudioClip AsteroidExplosion6 = null;
    public AudioClip DroidDamage1 = null;
    public AudioClip DroidDamage2 = null;
    public AudioClip DroidDamage3 = null;
    public AudioClip DroidLaser1 = null;
    public AudioClip DroidLaser2 = null;
    public AudioClip DroidLaser3 = null;
    public AudioClip KitSelection1 = null;
    public AudioClip KitSelection2 = null;
    public AudioClip KitSelection3 = null;
    public AudioClip PlayerDamage1 = null;
    public AudioClip PlayerDamage2 = null;
    public AudioClip PlayerDamage3 = null;
    public AudioClip PlayerLaser1 = null;
    public AudioClip PlayerLaser2 = null;
    public AudioClip PlayerLaser3 = null;

    // Fields
    private AudioSource _audioSource;
    private Dictionary<ClipType, List<AudioClip>> _sounds;

    /// <summary>
    /// Initializes the audio controller.
    /// </summary>
    private void Start()
    {
        // Create a reference to the AudioSource
        _audioSource = gameObject.GetComponent<AudioSource>();

        // Populate the sound dictionary
        _sounds = new Dictionary<ClipType, List<AudioClip>>();
        _sounds[ClipType.AsteroidExplosion] = new List<AudioClip> { AsteroidExplosion1, AsteroidExplosion2, AsteroidExplosion3, AsteroidExplosion4, AsteroidExplosion5, AsteroidExplosion6 };
        _sounds[ClipType.DroidDamage] = new List<AudioClip> { DroidDamage1, DroidDamage2, DroidDamage3 };
        _sounds[ClipType.DroidLaser] = new List<AudioClip> { DroidLaser1, DroidLaser2, DroidLaser3 };
        _sounds[ClipType.KitSelection] = new List<AudioClip> { KitSelection1, KitSelection2, KitSelection3 };
        _sounds[ClipType.PlayerDamage] = new List<AudioClip> { PlayerDamage1, PlayerDamage2, PlayerDamage3 };
        _sounds[ClipType.PlayerLaser] = new List<AudioClip> { PlayerLaser1, PlayerLaser2, PlayerLaser3 };
    }

    /// <summary>
    /// Plays a random audio clip given an input string.
    /// </summary>
    /// <param name="clipType">The type of sound that should be played.</param>
    public void PlayAudioClip(ClipType clipType)
    {
        _audioSource.PlayOneShot(SelectRandomClip(clipType));
    }

    private AudioClip SelectRandomClip(ClipType clipType)
    {
        if (clipType == ClipType.AsteroidExplosion)
        {
            return _sounds[clipType][Random.Range(0, 6)];
        }
        else
        {
            return _sounds[clipType][Random.Range(0, 3)];
        }
    }
}
