﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the game controller's behavior in Astro-Shooter
/// </summary>

using UnityEngine;

public class GameControllerBehavior : MonoBehaviour 
{
    // Properties
    public float SpawnRate = 0;
    public bool IsGameOver = false;
    public bool IsGamePaused = false;
    public bool OverrideSettingsStorage = false;

    // Enemy Information
    public GameObject Asteroid1 = null;
    public GameObject Asteroid2 = null;
    public GameObject Asteroid3 = null;
    public GameObject Asteroid4 = null;
    public GameObject Asteroid5 = null;
    public GameObject Asteroid6 = null;
    public GameObject Droid1    = null;

    // Fields
    private SpriteRenderer _spriteRenderer;
    private PlayerBehavior _player;

    // Droid Logic Fields
    private int _droidCount;
    private int _droidLimit;
    private bool _droidLimitIncremented;
    private int _nextScoreLimit;

    /// <summary>
    /// Initializes the game controller's spawning behavior.
    /// </summary>
    private void Start()
    {
        // Set the spawn rate to the one stored in the GameSettingStorage
        if (!OverrideSettingsStorage)
        {
            SpawnRate = GameSettingsStorage.SpawnRate;
        }

        // Get the GameController's SpriteRenderer for use in calculating enemy spawn points.
        _spriteRenderer = GetComponent<SpriteRenderer>();

        // Get a reference to the player object to keep track of the score.
        _player = GameObject.Find("Player").GetComponent<PlayerBehavior>();

        // Initialize the droid logic fields.
        _droidCount = 0;
        _droidLimit = 1;
        _droidLimitIncremented = false;
        _nextScoreLimit = _player.Score + 500;

        // Tell the collider to ignore any collisions with layers 0-1 (the background and GameController layers)
        Physics2D.IgnoreLayerCollision(0, 1, true);

        // Start a coroutine to start spawning enemies in accordance to the spawn rate.
        InvokeRepeating("SpawnEnemy", 0, SpawnRate);
	}

    /// <summary>
    /// Spawns an enemy at a random location into the game
    /// </summary>
    private void SpawnEnemy()
    {
        if (IsGameOver || IsGamePaused)
        {
            // Stop spawning enemies.
            StopCoroutine("SpawnEnemy");
        }
        else
        {
            // Determine if the max number of droids should increase.
            ControlDroidLimit();

            // Generate a new spawn point for the enemy.
            Vector2 spawnPoint = GenerateSpawnPoint();

            // Generate an enemy spawnID
            int spawnID = Random.Range(1, 8);

            // Analyze the int in a switch statement
            switch (spawnID)
            {
                case 1:
                    Instantiate(Asteroid1, spawnPoint, Quaternion.identity);
                    break;
                case 2:
                    Instantiate(Asteroid2, spawnPoint, Quaternion.identity);
                    break;
                case 3:
                    Instantiate(Asteroid3, spawnPoint, Quaternion.identity);
                    break;
                case 4:
                    Instantiate(Asteroid4, spawnPoint, Quaternion.identity);
                    break;
                case 5:
                    Instantiate(Asteroid5, spawnPoint, Quaternion.identity);
                    break;
                case 6:
                    Instantiate(Asteroid6, spawnPoint, Quaternion.identity);
                    break;
                case 7:
                    SpawnDroid1(spawnPoint);
                    break;
                default:
                    print("Error, please see GameControllerBehavior for more info");
                    break;
            }
        }      
    }

    /// <summary>
    /// Determines whether or not the maximum number of droids should increases or stay the same.
    /// </summary>
    private void ControlDroidLimit()
    {
        // Check if the player's score has gone over the score limit and the droid limit hasn't been increased.
        if (_player.Score != 0 && _player.Score >= _nextScoreLimit && !_droidLimitIncremented)
        {
            // Increase the droid limit.
            _droidLimitIncremented = true;
            _droidLimit++;

            // Increase the score limit.
            _nextScoreLimit += 500;
        }
        else
        {
            // Make sure the game controller knows the droid limit hasn't been increased since the last increase.
            _droidLimitIncremented = false;
        }
    }

    /// <summary>
    /// Calculates a random spawn point within the bounds of the game controller.
    /// </summary>
    /// <returns></returns>
    private Vector2 GenerateSpawnPoint()
    {
        // Make X1 the value of the Controllers center - the end / 2
        float X1 = transform.position.x - _spriteRenderer.bounds.size.x / 2;

        // Do the same for X2 with addition
        float X2 = transform.position.x + _spriteRenderer.bounds.size.x / 2;

        // Make a random spawn-point with the new X positions
        return new Vector2(Random.Range(X1, X2), transform.position.y);
    }

    /// <summary>
    /// Spawns a Droid1 enemy so long as the droid count does not exceed or equal the droid limit.
    /// </summary>
    /// <param name="spawnPoint">The spawn point of the droid.</param>
    private void SpawnDroid1(Vector2 spawnPoint)
    {
        // Make sure that another droid is allowed to spawn.
        if (_droidCount < _droidLimit)
        {
            // Increase the count of the number of droids.
            _droidCount++;

            // Spawn a Droid1 enemy.
            Instantiate(Droid1, spawnPoint, Quaternion.identity);
        }
    }

    /// <summary>
    /// Decrements the count of the total number of alive droids currently in the game.
    /// </summary>
    public void DecrementDroidCount()
    {
        _droidCount--;
    }  
}