﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the ammo kit's behavior in Astro-Shooter
/// </summary>

using UnityEngine;

public class HealthKitBehavior : KitBehavior
{
    // Properties
    public int HealthAmount = 0;
    public int ScoreAmount = 0;

    /// <summary>
    /// Automatically called by the editor.
    /// Gives health to the player once the player collides with the health kit.
    /// If the player already is at full health, 100 points are added to their score instead.
    /// </summary>
    /// <param name="ObjectCollidedWith">The object that collided with the health kit.</param>
    protected override void OnTriggerEnter2D(Collider2D ObjectCollidedWith)
    {
        // Check if the collider hit with the player or something else
        if (ObjectCollidedWith.gameObject.tag == "Player")
        {
            // Play a sound effect
            _gameAudioController.PlayAudioClip(ClipType.KitSelection);
            
            // Check if the player has more than 2 lives
            if (_player.Lives > 2)
            {
                // Give the player score instead of lives
                _player.Score += ScoreAmount;
            }
            else
            {
                // Give the player a life
                _player.Lives += HealthAmount;
            }

            // Destroy the health kit
            Destroy(gameObject);
        }
    }
}
