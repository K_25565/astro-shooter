﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/21/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the ammo kit's behavior in Astro-Shooter
/// </summary>

using UnityEngine;

public abstract class KitBehavior : MonoBehaviour
{
    // Properties
    public float LowerSpeedRange = 0;
    public float UpperSpeedRange = 0;

    // Fields
    protected AudioController _gameAudioController;
    protected GameControllerBehavior _gameController;
    protected PlayerBehavior _player;
    protected float _ySpeed;

    /// <summary>
    /// Initializes the kit's fields and starts any appropriate behaviors.
    /// </summary>
    protected void Start()
    {
        // Set the AudioController
        _gameAudioController = GameObject.Find("AudioController").GetComponent<AudioController>();

        // Set the GameControllerBehavior
        _gameController = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();

        // Set the PlayerBehavior
        _player = GameObject.Find("Player").GetComponent<PlayerBehavior>();

        // Set the kit's speed from the given range and ensure it is negative (falling).
        _ySpeed = Random.Range(LowerSpeedRange, UpperSpeedRange);
        _ySpeed = _ySpeed > 0 ? -_ySpeed : _ySpeed;
    }

    /// <summary>
    /// Called once a frame.
    /// Moves the kit in accordance to the MoveKit() function.
    /// </summary>
    protected void Update()
    {
        MoveKit();
    }

    /// <summary>
    /// Moves the kit down a random speed determined during the creation of the kit.
    /// </summary>
    protected void MoveKit()
    {
        // Check if the game is over
        if (_gameController.IsGameOver)
        {
            // Delete the health kit
            Destroy(gameObject);
        }
        else if (!_gameController.IsGamePaused)
        {
            // Check if the health kit is passed the kill line  
            if (transform.position.y < -1.5)
            {
                // Destroy the health kit
                Destroy(gameObject);
            }
            else
            {
                // Move the health kit down by the random speed
                transform.Translate(0f, _ySpeed * Time.deltaTime, 0f);
            }
        }
    }

    /// <summary>
    /// Abstract method that is automatically called by the editor.
    /// Describes the behavior of what happens when the player picks up this kit.
    /// </summary>
    /// <param name="ObjectCollidedWith"></param>
    protected abstract void OnTriggerEnter2D(Collider2D ObjectCollidedWith);
}