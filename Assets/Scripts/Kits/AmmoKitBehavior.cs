﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the ammo kit's behavior in Astro-Shooter
/// </summary>

using UnityEngine;

public class AmmoKitBehavior : KitBehavior
{
    // Properties
    public int AmmoAmount = 0;

    /// <summary>
    /// Automatically called by the editor.
    /// Gives the contents of the ammo kit to the player when the player collides with the kit.
    /// </summary>
    /// <param name="ObjectCollidedWith">The object that collided with the ammo kit</param>
    protected override void OnTriggerEnter2D(Collider2D ObjectCollidedWith)
    {
        // Check if the object that collided was the player
        if (ObjectCollidedWith.gameObject.tag == "Player")
        {
            // Play a sound effect
            _gameAudioController.PlayAudioClip(ClipType.KitSelection);
            
            // Add the AmmoAmount to the player's ammo
            _player.Ammo += AmmoAmount;

            // Destroy the ammo kit
            Destroy(gameObject);
        }
    }
}