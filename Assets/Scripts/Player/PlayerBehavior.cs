﻿/// <summary> 
/// Author:     Kenneth Gordon
/// Date:       05/23/2023
/// Copyright:  Licensed under the Apache License 2.0.  More info in LICENSE.md
/// 
/// This file contains a class that represents the player's behavior in Astro-Shooter
/// </summary>

using UnityEngine;

public class PlayerBehavior : MonoBehaviour 
{
    // Properties
    public int Score = 0;
    public int Lives = 3;
    public int Ammo = 30;
    public float XSpeed = 0;
    public float YSpeed = 0;
    public GameObject Weapon = null;
    public bool CanMoveForward = true;
    public bool CanMoveBack = true;
    public bool CanMoveRight = true;
    public bool CanMoveLeft = true;

    // Fields
    private AudioController _gameAudioController;
    private GameControllerBehavior _gameController;
    private bool _leaderboardWrittenTo;

    /// <summary>
    /// Initializes references to the game controllers and applies any custom settings to the game.
    /// </summary>
    private void Start()
    {
        // Set the AudioController and GameControllerBehavior references
        _gameAudioController = GameObject.Find("AudioController").GetComponent<AudioController>();
        _gameController = GameObject.Find("GameController").GetComponent<GameControllerBehavior>();

        // Initialize the _leaderboardWrittenTo field.
        _leaderboardWrittenTo = false;

        if (!_gameController.OverrideSettingsStorage)
        {
            Score = GameSettingsStorage.Score;
            Lives = GameSettingsStorage.Lives;
            Ammo = GameSettingsStorage.Ammo;
        }
    }

    /// <summary>
    /// Called once per frame.
    /// Checks for player movement, player weapon discharge, and whether or not the player has lost the game.
    /// </summary>
    private void Update()
    {
        CheckForGamePauseInput();

        if (!_gameController.IsGamePaused)
        {
            MovePlayer();
            FireWeapon();
            CheckForGameOver();
        }
    }

    /// <summary>
    /// Checks if the player has pressed the escape key.
    /// </summary>
    private void CheckForGamePauseInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _gameController.IsGamePaused = !_gameController.IsGamePaused;
        }
    }

    /// <summary>
    /// Moves the player in the given keyboard direction if the player is able to move in that direction.
    /// </summary>
    private void MovePlayer()
    {
        // Check if the player is able to move forward and has pressed 'w'
        if (CanMoveForward == true && Input.GetKey("w"))
        {
            // Move the object up by delta time and YSpeed
            transform.Translate(0f, YSpeed * Time.deltaTime, 0f);
        }

        // Check if the player is able to move backwards and has pressed 's'
        if (CanMoveBack == true && Input.GetKey("s"))
        {
            // Move the object down by delta time and YSpeed
            transform.Translate(0f, -(YSpeed) * Time.deltaTime, 0f);
        }

        // Check if the player is able to move left and has pressed 'a'
        if (CanMoveLeft == true && Input.GetKey("a"))
        {
            // Move the object left by delta time and XSpeed
            transform.Translate(-(XSpeed) * Time.deltaTime, 0f, 0f);
        }

        // Check if the player is able to move right and has pressed 'd'
        if (CanMoveRight == true && Input.GetKey("d"))
        {
            // Move the object right by delta time and XSpeed
            transform.Translate(XSpeed * Time.deltaTime, 0f, 0f);
        }           
    }

    /// <summary>
    /// Spawns a laser at the player's location if the player has pressed 'space', has ammo, and the game isn't over.
    /// </summary>
    private void FireWeapon()
    {
        // Only let the player shoot if the game isn't over and the player has ammo left
        if (!_gameController.IsGameOver && Ammo > 0 && Input.GetKeyDown("space"))
        {
            // Set the location to spawn the laser at 0.1 higher than the player
            Vector2 weaponLocation = new Vector2(transform.position.x, transform.position.y + 0.1f);

            // Remove an ammo
            Ammo -= 1;

            // Play a random laser sound effect
            _gameAudioController.PlayAudioClip(ClipType.PlayerLaser);

            // Spawn a new laser object
            Instantiate(Weapon, weaponLocation, Quaternion.identity);
        }
    }

    /// <summary>
    /// Checks if the player has either run out of lives or ammo.
    /// If they have, the game is over.
    /// </summary>
    private void CheckForGameOver()
    {
        // Check if the player's lives are less than 1 or if the player has ran out of ammo
        if (Lives < 1 || Ammo < 1)
        {
            // Activate game over mode
            GameObject.Find("GameController").GetComponent<GameControllerBehavior>().IsGameOver = true;

            // Set Lives and Ammo to 0 to make sure everything works correctly during game over mode
            Lives = 0;
            Ammo = 0;

            // Save the player's score to the leaderboard
            if (!_leaderboardWrittenTo && !GameSettingsStorage.IsCustomGame)
            {
                // Setup the LeaderboardManager's properties (just in case)
                LeaderboardManager.FilePath = $"{Application.persistentDataPath}/";
                LeaderboardManager.FileName = "leaderboard.astro";

                // Write the player's adjusted score to the leaderboard (don't allow the player to use a custom game to boost their score).
                LeaderboardManager.WriteToLeaderboard(Score);

                // Let the game controller know the leaderboard was written to and doesn't need to be written to again.
                _leaderboardWrittenTo = true;
            }

            // Hide the player
            GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    /// <summary>
    /// Automatically called by the editor.
    /// Runs the appropriate game logic depending on what object collided with the player
    /// </summary>
    /// <param name="ObjectCollidedWith">The object that collided with this player</param>
    private void OnTriggerEnter2D(Collider2D ObjectCollidedWith)
    {
        // Check if the object that hit the player is a droid's laser.
        if (ObjectCollidedWith.gameObject.tag == "DroidLaser")
        {
            // Play a random asteroid sound effect
            _gameAudioController.PlayAudioClip(ClipType.PlayerDamage);

            // Decrement the droid's health by the damage factor of the player's laser.
            Lives -= 1;

            // Play the red flash animation.
            gameObject.GetComponent<RedFlashAnimationBehavior>().PlayAnimation();

            // Destroy the colliding object
            Destroy(ObjectCollidedWith.gameObject);
        }

        // Asteroids collisions as well as droid collisions are handled within the AsteroidBehavior.cs and DroidBehavior.cs scripts respectively.
    }
}