# Astro-Shooter

```
Author:      Kenneth Gordon
Date:        23-May-2023
GitLab ID:   K_25565
Repo:        https://gitlab.com/K_25565/astro-shooter
Solution:    Astro-Shooter
Copyright:   Licensed under the Apache License 2.0.  More info in LICENSE.md
```

## Overview of Astro-Shooter

Astro-Shooter is a space shooter type game made in the Unity engine.  The goal of the game is to destroy as many asteroids as you can before either running out of ammo or lives.
This project was created as a fun way for me to refine my C# skills and to learn the Unity engine.

## Built With:

Astro-Shooter was built with the following tools:

* Unity:  Game Engine
* Visual Studio 2022:  Coding
* Paint.Net:  Artwork
* Adobe Photoshop Elements:  Artwork